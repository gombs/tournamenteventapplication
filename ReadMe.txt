Hollywood Bets

Technology used

- Entity Framework 6
- Autofac (Dependency inversion)
- SQL server-2012 (Database)
- C# .NetFramewwork Web-API 2
- Angular 8
- Material Design


Two main users have been created and exists in the seed.

The following are the system users or rather roles of players for thsi application

"Super User Administrator"

User 1 : UserName : superadmin@gmail.com
		 Password : 123456
		 
   - Capabilities
	* Can create the tournamanets.
	* Can create an Events.
	* View Event Details

"Administrator User"
	 
User 2 : UserName : admin@gmail.com
		 Password : 123456
   - Capabilities
	* Can only view the tournamanets created.
	* Can create an Event.
	
"Member"
User 3 : This user can be registered from the UI and is assigned a role 'Member'

   - Capabilities:
	* This user can create event details after selecting the tournament and the Event they are intrested in.