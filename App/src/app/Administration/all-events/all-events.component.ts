import { Component, OnInit } from '@angular/core';
import { IEventModel } from 'src/app/shared/models/event.Model';
import { TournamentService } from 'src/app/shared/services/tournament-service/tournament.service';

@Component({
  selector: 'app-all-events',
  templateUrl: './all-events.component.html',
  styleUrls: ['./all-events.component.css']
})
export class AllEventsComponent implements OnInit {

  public tournamentEventList: IEventModel[];
  constructor(private tournamentService :TournamentService) { }

  ngOnInit() {
    this.getEventsList();
  }

  private getEventsList(){
    this.tournamentService.getAllEventsList().subscribe( tournamentEvents => {
      this.tournamentEventList = tournamentEvents;
   });
  }

}
