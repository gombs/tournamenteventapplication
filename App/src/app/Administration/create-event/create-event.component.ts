import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { TournamentService } from 'src/app/shared/services/tournament-service/tournament.service';
import { ITournamentModel } from 'src/app/shared/models/tournament.Model';
import { IEventModel } from 'src/app/shared/models/event.Model';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  formErrors = {
    'tournamentId': '',
    'eventName':'',
    'autoClose':''
  };

  // This object contains all the validation messages for this form
  validationMessages = {
    'tournamentId': {
      'required': 'Tournament name is required.',
    },
    'eventName': {
      'required': 'Event name is required.',
      'minlength': 'Event name must be greater than 2 characters.',
      'maxlength': 'Event name must be less than 100 characters.'
    },
    'eventNumber': {
      'required': 'Event number is required.',
    },
    'eventDate': {
      'required': 'Event date is required.',
    },
    'eventEndDate': {
      'required': 'Event end date is required.',
    },
    'autoClose':{
      'required': 'Auto close confirmation is required.',
    }     
  };

  constructor( private toastr: ToastrService, private fb: FormBuilder, private tournamentService :TournamentService) { }

  public CreateEventForm: FormGroup;
  public tournamentList: ITournamentModel[];
  public tournamentEventList: IEventModel[];
  public EnableUpdate: boolean = false;
  
  ngOnInit() {
    this.getTournamentsList();
    this.getEventsList();

    this.CreateEventForm = this.fb.group({
      tournamentId: new FormControl('', [Validators.required]),
      eventName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
      eventNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      eventDate: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
      eventEndDate: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
      autoClose: new FormControl('', [Validators.required]),
    });

    this.CreateEventForm.valueChanges.subscribe((data) =>{
      this.logValidationErrors(this.CreateEventForm);
    });
  }

  OnSubmit(form: NgForm) {    
    let currentUser = "SuperAdmin@gmail.com"; // User service to provide this
    var eventData = form.value;
    this.tournamentService.CreateEvent(eventData, currentUser)
    .subscribe((data: any) => {
      if (data.StatusCode == 200) {
        this.getEventsList();
        this.CreateEventForm.reset();
        this.toastr.success('Successfully created a new Tournament', "Success!");
      }
      else{
        data.errors.forEach(element => {
              this.toastr.error('Tournament could not be created','Capture failed.');              
      })}
    })
  };

  UpdateEvent(tournamenEventId: number){
    this.tournamentService.getTournamentEventById(tournamenEventId).subscribe( (tournamentEvent: any) => {
      if(tournamentEvent != null)
      {
        this.CreateEventForm.controls['tournamentId'].setValue(tournamentEvent.TournamentId);
        this.CreateEventForm.controls['eventName'].setValue(tournamentEvent.Name);
        this.CreateEventForm.controls['eventNumber'].setValue(tournamentEvent.EventNumber);
        this.CreateEventForm.controls['eventDate'].setValue(tournamentEvent.EventDateTime);
        this.CreateEventForm.controls['eventEndDate'].setValue(tournamentEvent.EventEndDateTime);
        this.CreateEventForm.controls['autoClose'].setValue(tournamentEvent.AutoClose);
        this.EnableUpdate = true;
      } else{
        this.toastr.error('We could not find the event','Failed to locate event.');              
      }
    });
  }

  ResetCreatNeWEventForm()
  {
    this.CreateEventForm.reset();
    this.EnableUpdate = false;
  }

  private getTournamentsList(){
    this.tournamentService.getAllTournamentsList().subscribe( tournaments => {
      this.tournamentList = tournaments;
   });
  }

  private getEventsList(){
    this.tournamentService.getAllEventsList().subscribe( tournamentEvents => {
      this.tournamentEventList = tournamentEvents;
   });
  }

  // Logs validation error messages
  logValidationErrors(group: FormGroup = this.CreateEventForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      this.formErrors[key] = '';
      if (abstractControl && !abstractControl.valid
        && (abstractControl.touched || abstractControl.dirty)) {
        const messages = this.validationMessages[key];
        for (const errorKey in abstractControl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }
  
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      }
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.CreateEventForm.controls[controlName].hasError(errorName);
  }

}
