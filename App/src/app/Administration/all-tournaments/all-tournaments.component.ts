import { Component, OnInit } from '@angular/core';
import { ITournamentModel } from 'src/app/shared/models/tournament.Model';
import { TournamentService } from 'src/app/shared/services/tournament-service/tournament.service';

@Component({
  selector: 'app-all-tournaments',
  templateUrl: './all-tournaments.component.html',
  styleUrls: ['./all-tournaments.component.css']
})
export class AllTournamentsComponent implements OnInit {

  public tournamentList: ITournamentModel[];
  constructor( private tournamentService :TournamentService) { }

  ngOnInit() {
    this.getTournamentsList();
  }

  private getTournamentsList(){
    this.tournamentService.getAllTournamentsList().subscribe( tournamens => {
      this.tournamentList = tournamens;
   });
  }

}
