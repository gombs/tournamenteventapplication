import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { TournamentService } from 'src/app/shared/services/tournament-service/tournament.service';
import { ITournamentModel } from 'src/app/shared/models/tournament.Model';
import { IEventModel } from 'src/app/shared/models/event.Model';
import { filter } from 'minimatch';
import { map } from 'rxjs/operators';
import { IEventStatusModel, EventStatusModel } from 'src/app/shared/models/eventStatuses.model';
import { IAcceptModel, AcceptModel } from 'src/app/shared/models/accept.model';

@Component({
  selector: 'app-create-event-detail',
  templateUrl: './create-event-detail.component.html',
  styleUrls: ['./create-event-detail.component.css']
})
export class CreateEventDetailComponent implements OnInit {

  formErrors = {
    'tournamentName': '',
  };

  // This object contains all the validation messages for this form
  validationMessages = {
    'tournamentId': {
      'required': 'Tournament name is required.',
      'minlength': 'Tournament name must be greater than 2 characters.',
      'maxlength': 'Tournament name must be less than 100 characters.'
    },
    'tournamentEventId': {
      'required': 'Tournament event name is required.',
      'minlength': 'Tournament event name must be greater than 2 characters.',
      'maxlength': 'Tournament event name must be less than 100 characters.'
    },  
    'eventDetailName': {
      'required': 'Event detail name is required.',
      'minlength': 'Event detail name must be greater than 2 characters.',
      'maxlength': 'Event detail name must be less than 100 characters.'
    },  
    'eventDetailNumber': {
      'required': 'Event detail number is required.',
      'minlength': 'Event detail number must be greater than 2 characters.',
      'maxlength': 'Event detail number must be less than 100 characters.'
    },
    'eventDetailOdd': {
      'required': 'Event detail odd is required.',
      'minlength': 'Event detail odd must be greater than 2 characters.',
      'maxlength': 'Event detail odd must be less than 100 characters.'
    },
    'finishingPosition': {
      'required': 'Finishing position is required.',
      'minlength': 'Finishing position must be greater than 2 characters.',
      'maxlength': 'Finishing position must be less than 100 characters.'
    },
    'eventDetailStatus': {
      'required': 'Event detail status is required.',
      'minlength': 'Event detail status must be greater than 2 characters.',
      'maxlength': 'Event detail status must be less than 100 characters.'
    },
    'firstTime': {
      'required': 'First time is required.',
      'minlength': 'First time value must be greater than 1 characters.',
      'maxlength': 'First time value must be less than 100 characters.'
    },
  };

  constructor( private toastr: ToastrService, private fb: FormBuilder, private tournamentService :TournamentService) { }

  public CreateEventDetailForm: FormGroup;
  public tournamentList: ITournamentModel[];
  public eventList: IEventModel[];
  public eventStatuses: IEventStatusModel[] = [new EventStatusModel(1,'Active'),
                                               new EventStatusModel(2,'Scratched'),
                                               new EventStatusModel(3,'Closed')];
  public isFirstTime: AcceptModel[] = [ new AcceptModel( 1,'Yes'),
                                         new AcceptModel( 0, 'No')]
    
  ngOnInit() {

    this.getTournamentsList();

    this.CreateEventDetailForm = this.fb.group({
      tournamentId: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]), 
      tournamentEventId: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      eventDetailName: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]),
      eventDetailNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      eventDetailOdd: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      finishingPosition: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      eventDetailStatus: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      firstTime: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]),
    });

    this.CreateEventDetailForm.valueChanges.subscribe((data) =>{
      this.logValidationErrors(this.CreateEventDetailForm);
    });
  }

  //Get all events and filter tem based on the first selection
  onSelectTournamentChange(event): void  {
    this.CreateEventDetailForm.controls['tournamentEventId'].setValue('');
    this.tournamentService.getAllEventsList().pipe(
      map(items => items.filter(item => item.TournamentId == event.value))
    ).subscribe( result => this.eventList = result);
  }

  private getTournamentsList(){
    this.tournamentService.getAllTournamentsList().subscribe( tournamens => {
      this.tournamentList = tournamens;
   });
  }


  // Logs validation error messages
  logValidationErrors(group: FormGroup = this.CreateEventDetailForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      this.formErrors[key] = '';
      if (abstractControl && !abstractControl.valid
        && (abstractControl.touched || abstractControl.dirty)) {
        const messages = this.validationMessages[key];
        for (const errorKey in abstractControl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }
  
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      }
    });
  }

  public hasError = (controlName: string, errorName: string) =>{
    return this.CreateEventDetailForm.controls[controlName].hasError(errorName);
  }

  
  OnSubmit(form: NgForm) {    
    let currentUser = "SuperAdmin@gmail.com"; // User service to provide this
    var TournamentEvent = form.value;
    this.tournamentService.CreateEventDetail(TournamentEvent,currentUser)
    .subscribe((data: any) => {
      if (data.StatusCode == 200) {
        this.CreateEventDetailForm.reset();
        this.toastr.success('Successfully created a new event detail', "Success!");
      }
      else{
            this.toastr.error('Event Detail failed capturing','Capture failed.');
      }
    });  
  }

}
