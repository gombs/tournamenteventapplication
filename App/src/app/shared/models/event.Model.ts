export interface IEventModel{ 
    EventId:number,          
    Name: string,
    EventNumber: number,
    EventDateTime: string,
    EventEndDate: Date,
    TournamentId: number,
    TournamentName: string,
    AutoClose: number
}

export class EventModel {
    constructor(
        public EventId:number,
        public Name: string,
        public EventNumber: number,
        public EventDateTime: string,
        public EventEndDateTime: Date,
        public tournamentId: number,
        public TournamentName: string,
        public AutoClose: number      
        ) { }
}
