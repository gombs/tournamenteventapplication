export interface IEventStatusModel{  
    Id: number,  
    StatusName: string
}

export class EventStatusModel {
    constructor(
        public Id: number,
        public StatusName: string) { }
}
