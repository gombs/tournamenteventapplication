export interface ITournamentModel{  
    Id: number,  
    tournamentName: string,
    createdBy: string
}

export class TournamentModel {
    constructor(
        public Id: number,
        public tournamentName: string,
        public createdBy: string) { }
}
