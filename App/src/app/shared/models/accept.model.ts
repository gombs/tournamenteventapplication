export interface IAcceptModel{
    id: number,
    accept: string
}


export class AcceptModel {
    constructor(
        public Id: number,
        public accept: string) { }
}