using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using TournamentData;
using TournamentData.ApplicationTypes;
using TournamentData.Commands.EventsCommand;
using TournamentData.Context;
using TournamentData.DomainModels;
using TournamentData.Enums;
using TournamentData.Models;
using TournamentData.SystemMessages;

namespace Tournament.Controllers
{
  public class EventsController : ApiController
    {
      private readonly TournamentDbContext _context;
      private readonly ITournamentDomainClient _tournamentDomainClient;

      public EventsController(TournamentDbContext context, ITournamentDomainClient tournamentDomainClient)
      {
          _context = context;
          _tournamentDomainClient = tournamentDomainClient;
      }

      [System.Web.Http.HttpGet]
      [System.Web.Http.Route("api/events")]
      [System.Web.Http.AllowAnonymous]
      public IEnumerable<TournamentEvent> GetEvents()
      {
        var tournamentEvents = new List<TournamentEvent>();
        var events = _context.Event.ToList();
        foreach (var retrievedEvent in events)
        {
          var eventItem = new TournamentEvent()
          {
              EventId = retrievedEvent.Id,
              Name = retrievedEvent.Name,
              EventNumber = retrievedEvent.EventNumber,
              EventDateTime = retrievedEvent.EventDateTime,
              EventEndDateTime = retrievedEvent.EventEndDateTime,
              AutoClose = retrievedEvent.AutoClose,
              TournamentId = retrievedEvent.TournamentId
          };

          tournamentEvents.Add(eventItem);
        }

        return tournamentEvents;
      }
    
      [System.Web.Http.HttpPost]
      [System.Web.Http.Route("api/event/create")]
      [System.Web.Http.AllowAnonymous]
      public ActionResult CreateEvent(TournamentEvent tournamentEvent)
      {
          ISystemResponseMessages systemMessages = new SystemResponseMessages(ApplicationResponseMessagesEnum.NoAction, "");
          
          var command = new CreateEventCommand(tournamentEvent, _context);
          _tournamentDomainClient.PerformCommand(command, out systemMessages);

          if (systemMessages.MessageState().Equals(ApplicationResponseMessagesEnum.Success))
          {
            return new HttpStatusCodeResult(HttpStatusCode.OK, "Event created");
          }

          return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Event not captured");
      }

    [System.Web.Http.HttpGet]
    [System.Web.Http.Route("api/events/id")]
    [System.Web.Http.AllowAnonymous]
    public TournamentEvent GetEvent([FromUri]int id)
    {
      var tournamentEvent = new TournamentEvent();
      var events = _context.Event.ToList();
      var eventElement = events.Single(e => e.Id == id);
      if (eventElement != null)
      {
        tournamentEvent.EventId = eventElement.Id;
        tournamentEvent.EventNumber = eventElement.EventNumber;
        tournamentEvent.EventDateTime = eventElement.EventDateTime;
        tournamentEvent.EventEndDateTime = eventElement.EventEndDateTime;
        tournamentEvent.AutoClose = eventElement.AutoClose;
        tournamentEvent.Name = eventElement.Name;
      }

       return tournamentEvent;
    }
  }
}
