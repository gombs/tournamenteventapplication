using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TournamentData;
using TournamentData.ApplicationTypes;
using TournamentData.Commands.EventDetailCommands;
using TournamentData.Context;
using TournamentData.DomainModels;
using TournamentData.Enums;
using TournamentData.SystemMessages;

namespace Tournament.Controllers
{
   public class EventDetailsController : ApiController
   {
      private readonly TournamentDbContext _context;
      private readonly ITournamentDomainClient _tournamentDomainClient;

     public EventDetailsController(TournamentDbContext context, ITournamentDomainClient tournamentDomainClient)
     {
        _context = context;
        _tournamentDomainClient = tournamentDomainClient;
     }


      [System.Web.Http.HttpPost]
      [System.Web.Http.Route("api/eventdetails/create")]
      [System.Web.Http.AllowAnonymous]
      public ActionResult CreateEventDetail(EventDetail eventDetail)
      {
          ISystemResponseMessages systemMessages = new SystemResponseMessages(ApplicationResponseMessagesEnum.NoAction, "");

          var command = new CreateEventDetailCommand(eventDetail, _context);
          _tournamentDomainClient.PerformCommand(command, out systemMessages);

          if (systemMessages.MessageState().Equals(ApplicationResponseMessagesEnum.Success))
          {
            return new HttpStatusCodeResult(HttpStatusCode.OK, "Event created");
          }

          return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Event not captured");
      }



  }
}
