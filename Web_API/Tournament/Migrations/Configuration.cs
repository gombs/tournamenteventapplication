using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Tournament.Models;
using System;
using System.Data.Entity.Migrations;

namespace Tournament.Migrations
{
  internal sealed class Configuration : DbMigrationsConfiguration<Tournament.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Tournament.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Roles.AddOrUpdate(x => x.Id,
              new IdentityRole() { Id = "1", Name = "SuperAdmin" },
              new IdentityRole() { Id = "2", Name = "Admin" },
              new IdentityRole() { Id = "3", Name = "Member" }
            );
            var PasswordHash = new PasswordHasher();

            // user 2 Administrator
            var AdminSuperUser = new ApplicationUser() { FirstName = "SuperAdministrator", LastName = "HollywoodSuperAdmin", Email = "superadmin@gmail.com", Initials = "HA" };
            AdminSuperUser.PasswordHash = PasswordHash.HashPassword("123456");
            AdminSuperUser.SecurityStamp = Guid.NewGuid().ToString();

            var adminSuperRole1 = new IdentityUserRole();
            adminSuperRole1.RoleId = "1";
            adminSuperRole1.UserId = AdminSuperUser.Id;

            var adminSuperRole2 = new IdentityUserRole();
            adminSuperRole2.RoleId = "2";
            adminSuperRole2.UserId = AdminSuperUser.Id;

            AdminSuperUser.Roles.Add(adminSuperRole1);
            AdminSuperUser.Roles.Add(adminSuperRole2);
            context.Users.Add(AdminSuperUser);

            // user 2 Administrator
            var AdminUser = new ApplicationUser(){ FirstName = "Administrator", LastName = "HollywoodAdmin", Email = "admin@gmail.com", Initials = "HA"};
            AdminUser.PasswordHash = PasswordHash.HashPassword("123456");
            AdminUser.SecurityStamp = Guid.NewGuid().ToString();

            var adminRole = new IdentityUserRole();
            adminRole.RoleId = "2";
            adminRole.UserId = AdminUser.Id;

            AdminUser.Roles.Add(adminRole);
            context.Users.Add(AdminUser);
        }
    }
}
