﻿namespace TournamentData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewStartcenterofdatabasecreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EventNumber = c.Int(nullable: false),
                        EventDateTime = c.DateTime(nullable: false),
                        EventEndDateTime = c.DateTime(nullable: false),
                        AutoClose = c.Int(nullable: false),
                        TournamentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TournamentModels", t => t.TournamentId, cascadeDelete: true)
                .Index(t => t.TournamentId);
            
            CreateTable(
                "dbo.EventDetailModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventDetailName = c.String(),
                        EventDetailNumber = c.String(),
                        EventDateTime = c.DateTime(nullable: false),
                        EventEndDateTime = c.DateTime(nullable: false),
                        EventDetailOdd = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AutoClose = c.Int(nullable: false),
                        EventDetailStatus = c.Int(nullable: false),
                        FirstTime = c.Int(nullable: false),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventModels", t => t.Event_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.TournamentModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventDetailStatusModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventDetailStatusName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventModels", "TournamentId", "dbo.TournamentModels");
            DropForeignKey("dbo.EventDetailModels", "Event_Id", "dbo.EventModels");
            DropIndex("dbo.EventDetailModels", new[] { "Event_Id" });
            DropIndex("dbo.EventModels", new[] { "TournamentId" });
            DropTable("dbo.EventDetailStatusModels");
            DropTable("dbo.TournamentModels");
            DropTable("dbo.EventDetailModels");
            DropTable("dbo.EventModels");
        }
    }
}
