using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData.ApplicationModels;
using TournamentData.ApplicationTypes;
using TournamentData.Context;
using TournamentData.Enums;
using TournamentData.SystemMessages;

namespace TournamentData.Commands.TournamentCommand
{
  public class UpdateTournamentCommand : ICommand
  {
    private Tournament _tournamentModel;
    private IDataRepository<Tournament> _tournamentDataRepository;

    public UpdateTournamentCommand(Tournament tournament, TournamentDbContext context)
    {
      _tournamentModel = tournament;
      _tournamentDataRepository = new TournamentDataRespository(context);
    }

    public void HandleCommand(out ISystemResponseMessages responseMessage)
    {
      var tournamentsList = _tournamentDataRepository.GetAll();
      var doesTournamentExists = DoesTournamentExist(tournamentsList, (int)_tournamentModel.Id);

      if (doesTournamentExists)
      {
        try
        {
          _tournamentDataRepository.Update(_tournamentModel);
          responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Success, "Tournament Created");
        }
        catch (Exception ex)
        {
          responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Failure, "Error Occured : " + ex);
        }
      }else
      {
        responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Failure, "Error Occured : ");
      }

    }

    private bool DoesTournamentExist(IEnumerable<Tournament> TournamentList, int id)
    {
      return TournamentList.Any(x => x.Id == id);
    }
  }
}
