using System.Collections.Generic;
using System.Linq;
using TournamentData.ApplicationTypes;
using TournamentData.Context;
using TournamentData.DomainModels;

namespace TournamentData.Commands.EventDetailCommands
{
  public class EventDetailsRepository : IDataRepository<EventDetail>
  {
    public readonly TournamentDbContext _coneTournamentDbContext;
    public EventDetailsRepository(TournamentDbContext context)
    {
      _coneTournamentDbContext = context;
    }

    public void Create(EventDetail dataModel)
    {
      using (_coneTournamentDbContext)
      {
        var eventModel = _coneTournamentDbContext.Event.Find(dataModel.EventId);

        var eventDetailModel = new Models.EventDetailModel();
        eventDetailModel.EventDetailName = dataModel.EventDetailName;
        eventDetailModel.EventDetailNumber = dataModel.EventDetailNumber;
        eventDetailModel.EventDateTime = dataModel.EventDateTime;
        eventDetailModel.EventDetailOdd = dataModel.EventDetailOdd;
        eventDetailModel.FinishingPosition = dataModel.FinishingPosition;
        eventDetailModel.EventId = dataModel.EventId;
        eventDetailModel.EventDetailStatusId = dataModel.EventDetailStatusId;
        _coneTournamentDbContext.EventDetailModel.Add(eventDetailModel);
        _coneTournamentDbContext.SaveChanges();
      }
    }

    public void Delete(int Id)
    {
      using (_coneTournamentDbContext)
      {
        var modelToDelete = _coneTournamentDbContext.EventDetailModel.Find(Id);
        _coneTournamentDbContext.EventDetailModel.Remove(modelToDelete);
        _coneTournamentDbContext.SaveChanges();
      }
    }

    public IEnumerable<EventDetail> GetAll()
    {
      var eventDetails = new List<EventDetail>();

      var eventDetailsList = _coneTournamentDbContext.EventDetailModel.ToList();
      foreach (var eventDetailRetrieved in eventDetailsList)
      {
        eventDetails.Add(new EventDetail()
        {
            Id  = eventDetailRetrieved.Id,
            EventDetailName  = eventDetailRetrieved.EventDetailName,
            EventDetailNumber  = eventDetailRetrieved.EventDetailNumber,
            EventDateTime  = eventDetailRetrieved.EventDateTime,
            FinishingPosition = eventDetailRetrieved.FinishingPosition,
            EventDetailOdd  = eventDetailRetrieved.EventDetailOdd,
            FirstTime  = eventDetailRetrieved.FirstTime,
            EventId = eventDetailRetrieved.Event.Id
        });
      }

      return eventDetails;
    }

    public EventDetail GetById(int Id)
    {
      throw new System.NotImplementedException();
    }

    public void Save()
    {
      throw new System.NotImplementedException();
    }

    public void Update(EventDetail DataModel)
    {
      throw new System.NotImplementedException();
    }
  }
}
