using System;
using System.Collections.Generic;
using System.Linq;
using TournamentData.ApplicationTypes;
using TournamentData.Context;
using TournamentData.DomainModels;
using TournamentData.Enums;
using TournamentData.SystemMessages;

namespace TournamentData.Commands.EventDetailCommands
{
  public class CreateEventDetailCommand : ICommand
  {
    private EventDetail _eventDetailsModel;
    private IDataRepository<EventDetail> _eventDetailsRepository;

    public CreateEventDetailCommand(EventDetail eventDetailModel, TournamentDbContext context)
    {
      _eventDetailsModel = eventDetailModel;
      _eventDetailsRepository = new EventDetailsRepository(context);
    }

    public void HandleCommand(out ISystemResponseMessages responseMessage)
    {
      var eventsDetailsList = _eventDetailsRepository.GetAll();
      var doesEventDetailExists = DoesEventDetailExist(eventsDetailsList);

      if (doesEventDetailExists)
      {
        responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Failure, "Event Detail Already exists.");
        return;
      }

      try
      {
        _eventDetailsRepository.Create(_eventDetailsModel);
        responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Success, "Event Detail Created");
      }
      catch (Exception ex)
      {
        responseMessage = new SystemResponseMessages(ApplicationResponseMessagesEnum.Failure, "Error Occured : " + ex);
      }
    }

    private bool DoesEventDetailExist(IEnumerable<EventDetail> eventsDetailList)
    {
      return eventsDetailList.Any(x => x.EventDetailName == _eventDetailsModel.EventDetailName);
    }
  }
}
