using System.ComponentModel.DataAnnotations.Schema;

namespace TournamentData.Models
{
  public class EventDetailStatusModel
  {
    [ForeignKey("EventDetailModel")]
    public int Id { get; set; }
    public string EventDetailStatusName { get; set; }

    public virtual EventDetailModel EventDetailModel { get; set; }
  }
}
