using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TournamentData.Models
{
  public class EventDetailModel
  {
    public int Id { get; set; }
    public int EventId { get; set; }
     public int EventDetailStatusId { get; set; }
    public string EventDetailName { get; set; }
    public string EventDetailNumber { get; set; }
    public DateTime EventDateTime { get; set; }
    public Decimal EventDetailOdd { get; set; }
    public int FinishingPosition { get; set; }
    public int FirstTime { get; set; }
    public virtual EventModel Event { get; set; }
    public virtual EventDetailStatusModel EventDetailStatus { get; set; }
  }
}
