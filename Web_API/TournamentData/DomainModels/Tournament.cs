using TournamentData.ApplicationTypes;

namespace TournamentData.ApplicationModels
{
  public class Tournament : IDataModel
  {
    public Tournament(int? id, string tournamentName)
    {
      Id = id;
      Name = tournamentName;
    }
    public int?  Id { get; set; }

    public string Name { get; set; }
  }
}
