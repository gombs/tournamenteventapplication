using System;
using TournamentData.ApplicationTypes;
using TournamentData.Models;

namespace TournamentData.DomainModels
{
  public class EventDetail : IDataModel
  {
    public int Id { get; set; }
    public int tournamentId { get; set; }
    public int EventId { get; set; }
    public string EventDetailName { get; set; }
    public string EventDetailNumber { get; set; }
    public DateTime EventDateTime { get; set; }
    public Decimal EventDetailOdd { get; set; }
    public int FinishingPosition { get; set; }
    public int FirstTime { get; set; }
    public int EventDetailStatusId { get; set; }
  }
}
